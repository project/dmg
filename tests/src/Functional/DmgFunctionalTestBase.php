<?php

namespace Drupal\Tests\dmg\Functional;

use Drupal\Core\Url;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;

/**
 * Defines a base class for our functional tests.
 */
abstract class DmgFunctionalTestBase extends BrowserTestBase {

  use ContentModerationTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $profile = 'testing';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An admin user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * Permissions to grant the admin user.
   *
   * @var array
   */
  protected $adminPermissions = [
    'access administration pages',
    'administer site configuration',
    'administer display modes',
    'administer content types',
    'administer node form display',
    'administer node display',
    'administer node fields',
    'administer taxonomy',
    'administer taxonomy_term display',
    'administer taxonomy_term form display',
    'administer taxonomy_term fields',
    'administer users',
    'administer account settings',
    'administer user display',
    'administer user form display',
    'administer user fields',
    'bypass node access',
  ];

  /**
   * The editorial workflow entity.
   *
   * @var \Drupal\workflows\Entity\Workflow
   */
  protected $workflow;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'block', 'taxonomy', 'user', 'field_ui', 'dmg'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('local_actions_block', ['id' => 'actions_block']);

    $this->adminUser = $this->drupalCreateUser(
      $this->adminPermissions,
      $this->getRandomGenerator()->name(),
      TRUE
    );
  }

  /**
   * Creates a content type from the UI.
   *
   * @param string $content_type_name
   *   Content type human name.
   * @param string $content_type_id
   *   Machine name.
   */
  protected function createContentTypeFromUi(string $content_type_name, string $content_type_id): void {
    $this->drupalGet('admin/structure/types');
    $this->clickLink('Add content type');

    $edit = [
      'name' => $content_type_name,
      'type' => $content_type_id,
    ];
    $this->submitForm($edit, 'op');

    // Check the content type has been set to create new revisions.
    $this->assertTrue(NodeType::load($content_type_id)
      ->shouldCreateNewRevision());
  }

  /**
   * Add creation guidelines from the UI.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $machine_name
   *   The machine name of the display mode.
   * @param string $guidelines
   *   The guidelines.
   * @param string $display_mode_type
   *   Either 'view' or 'form'.
   */
  protected function configureUsageGuidelinesThroughUi(string $entity_type_id, string $machine_name, string $guidelines, string $display_mode_type = 'view'): void {
    $this->drupalGet('admin/structure/display-modes/' . $display_mode_type . '/add/' . $entity_type_id);
    $edit = [
      'guidelines[value]' => $guidelines,
      'guidelines[format]' => 'restricted_html',
    ];
    $this->submitForm($edit, 'Save');
  }

  /**
   * Add creation guidelines from the UI.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $guidelines
   *   The guidelines.
   * @param string $display_mode_type
   *   Either 'view' or 'form'.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  protected function configureCreationGuidelinesThroughUi(string $entity_type_id, string $guidelines, string $display_mode_type = 'view'): void {
    $assert_session = $this->assertSession();
    $url = Url::fromRoute(
      'dmg.settings',
      ['type' => $display_mode_type, 'entity_type_id' => $entity_type_id]
    );
    $this->drupalGet($url);
    $assert_session->statusCodeEquals(200);
    $edit = [
      'entity_type_id' => $entity_type_id,
      'guidelines[value]' => $guidelines,
    ];
    $this->submitForm($edit, 'Save configuration');
    $assert_session->pageTextContains('The configuration options have been saved.');
  }

  /**
   * Create the display mode through the UI.
   *
   * @param string $display_mode_type
   *   Either 'view' or 'form'.
   * @param string $entity_type_id
   *   The entity type ID this view mode is for.
   * @param string $display_mode_name
   *   The name of the view mode.
   * @param string $guidelines
   *   The guidelines.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function createDisplayModeThroughUi(string $display_mode_type, string $entity_type_id, string $display_mode_name, string $guidelines): void {
    $assert_session = $this->assertSession();
    $url = Url::fromRoute(
      "entity.entity_{$display_mode_type}_mode.add_form",
      ['entity_type_id' => $entity_type_id]
    );
    $this->drupalGet($url);
    $edit = [
      'label' => $display_mode_name,
      'id' => preg_replace('/[^a-z\d]+/', '_', mb_strtolower($display_mode_name)),
      'guidelines[value]' => $guidelines,
    ];
    $this->submitForm($edit, 'Save');
    $message = sprintf('Saved the %s %s mode.', $display_mode_name, $display_mode_type);
    $assert_session->pageTextContains($message);
  }

}
