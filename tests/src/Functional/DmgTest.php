<?php

namespace Drupal\Tests\dmg\Functional;

use Drupal\Core\Url;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests preview on entity_reference integration.
 *
 * @group dmg
 */
class DmgTest extends DmgFunctionalTestBase {

  use TaxonomyTestTrait;

  /**
   * Test the preview formatter when checking the "latest" entity route.
   */
  public function testGuidelines(): void {
    $this->drupalLogin($this->adminUser);
    $this->drupalCreateContentType(['type' => 'article']);
    $this->createVocabulary(['type' => 'tags']);

    $entity_type_ids = ['node', 'user', 'taxonomy_term'];
    $types = ['view', 'form'];
    foreach ($entity_type_ids as $entity_type_id) {
      foreach ($types as $type) {
        $this->testCreationGuidelines($entity_type_id, $type);
      }
    }
    $this->testArticleUsageGuidelines('node', 'view');
  }

  /**
   * Tests the creation guidelines.
   */
  private function testCreationGuidelines(string $entity_type_id, string $display_mode_type): void {
    $assert_session = $this->assertSession();
    // Enable the creation guidelines and check that they are shown.
    $guidelines = 'These are the guidelines for the tests.';
    $this->configureCreationGuidelinesThroughUi(
      $entity_type_id,
      $guidelines,
      $display_mode_type,
    );
    $url = Url::fromRoute(
      "entity.entity_{$display_mode_type}_mode.add_form",
      ['entity_type_id' => $entity_type_id]
    );
    $this->drupalGet($url);
    $assert_session->elementTextContains('css', '.messages.messages--warning', $guidelines);
  }

  /**
   * Tests the usage guidelines.
   */
  private function testArticleUsageGuidelines(string $entity_type_id, string $display_mode_type): void {
    // Prepare the content type we will be using.
    $this->createContentTypeFromUi('Article', 'article');
    /** @var \Behat\Mink\Element\DocumentElement $page */
    $page = $this->getSession()->getPage();
    /** @var \Drupal\Tests\WebAssert $assert_session */
    $assert_session = $this->assertSession();
    // Enable the creation guidelines and check that they are shown.
    $guidelines = 'Some more usage guidelines for the tests.';
    $display_mode_name = 'My Display Mode';
    $this->createDisplayModeThroughUi(
      $display_mode_type,
      $entity_type_id,
      $display_mode_name,
      $guidelines,
    );
    $assert_session->pageTextContains($guidelines);
    // Now visit the manage display page to check for the alert.
    $this->drupalGet('/admin/structure/types/manage/article/display');
    $page->checkField('display_modes_custom[my_display_mode]');
    $page->pressButton('Save');
    $page->clickLink('configure them');
    $assert_session->elementTextContains('css', '.messages.messages--warning', $guidelines);
  }

}
