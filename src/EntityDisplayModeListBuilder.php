<?php

namespace Drupal\dmg;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\field_ui\EntityDisplayModeListBuilder as CoreEntityDisplayModeListBuilder;

/**
 * Overrides class for building a listing of view mode entities.
 */
class EntityDisplayModeListBuilder extends CoreEntityDisplayModeListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $headers = parent::buildHeader();
    $operations = array_pop($headers);
    $headers[] = $this->t('Guidelines');
    $headers[] = $operations;
    return $headers;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = parent::buildRow($entity);
    $operations = array_pop($row);
    $guidelines = $entity->getThirdPartySetting('dmg', 'guidelines', $this->t('<em>- No guidelines configured yet -</em>'));
    $row[]['data'] = ['#markup' => Xss::filterAdmin($guidelines)];
    $row[] = $operations;
    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $entity_types = array_keys($this->load());
    $type = str_replace(['entity_', '_mode'], '', $this->entityTypeId);
    $settings = \Drupal::config('dmg.settings')->get($type);
    foreach ($entity_types as $entity_type) {
      if (!isset($this->entityTypes[$entity_type])) {
        continue;
      }

      // Filter entities.
      if (!$this->isValidEntity($entity_type)) {
        continue;
      }
      $table = &$build[$entity_type];
      $has_creation_guidelines = count(array_filter($settings, static fn(array $s) => $s['entity_type_id'] === $entity_type)) !== 0;
      $message = $has_creation_guidelines
        ? $this->t('Update creation guidelines for @entity-type %label', [
          '@entity-type' => $this->entityTypes[$entity_type]->getLabel(),
          '%label' => $this->entityType->getSingularLabel(),
        ])
        : $this->t('Add creation guidelines for @entity-type %label', [
          '@entity-type' => $this->entityTypes[$entity_type]->getLabel(),
          '%label' => $this->entityType->getSingularLabel(),
        ]);
      $table['#rows']['_configure_guidelines'][] = [
        'data' => [
          '#type' => 'link',
          '#url' => Url::fromRoute('dmg.settings', [
            'type' => $type,
            'entity_type_id' => $this->entityTypes[$entity_type]->id(),
          ]),
          '#title' => $message,
        ],
        'colspan' => count($table['#header']),
      ];
    }
    return $build;
  }

}
