<?php

namespace Drupal\dmg\Form;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Component Libraries: Blocks settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    $entity_type_manager = $container->get('entity_type.manager');
    $config_factory = $container->get('config.factory');
    $typed_config_manager = $container->get('config.typed');

    assert($entity_type_manager instanceof EntityTypeManagerInterface);
    assert($config_factory instanceof ConfigFactoryInterface);
    assert($typed_config_manager instanceof TypedConfigManagerInterface);
    return new static(
      $config_factory,
      $typed_config_manager,
      $entity_type_manager,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'dmg_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $type = 'view', string $entity_type_id = ''): array {
    $plugin_id = sprintf('entity_%s_mode', $type);
    try {
      $display_mode = $this->entityTypeManager->getDefinition($plugin_id);
    }
    catch (PluginNotFoundException $e) {
      $this->messenger()->addError('The requested plugin type does not exist.');
      return parent::buildForm($form, $form_state);
    }
    $entity_type_id = $form_state->getValue('entity_type_id') ?? $form_state->getUserInput()['entity_type_id'] ?? $entity_type_id;
    try {
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    }
    catch (PluginNotFoundException $e) {
    }
    $settings = $this->config('dmg.settings');
    // See if there is a value in the form state.
    $guidelines = $form_state->getValue(['guidelines', 'value']);
    if (empty($guidelines)) {
      // If not, see if there are active settings for this.
      $matches = array_filter(
        $settings->get($type),
        static fn($s) => ($s['entity_type_id'] ?? '') === $entity_type_id,
      );
      $index = key($matches);
      if (isset($index)) {
        $guidelines = $settings->get(sprintf('%s.%d.guidelines', $type, $index));
      }
    }
    $form['info'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => $this->t(
        'Creation guidelines for %mode',
        ['%mode' => $display_mode->getLabel()],
      ),
    ];
    $form['type'] = ['#type' => 'hidden', '#value' => $type];
    $options = array_map(
      static fn(EntityTypeInterface $entity_type) => $entity_type->getLabel(),
      array_filter(
        $this->entityTypeManager->getDefinitions(),
        static fn(EntityTypeInterface $entity_type) => $entity_type instanceof ContentEntityTypeInterface
      ),
    );
    $form['entity_type_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Type'),
      '#description' => $this->t('The creation guidelines will show when creating a new display mode for this entity type.'),
      '#empty_option' => $this->t('- Select one -'),
      '#options' => $options,
      '#required' => TRUE,
      '#default_value' => isset($entity_type) ? $entity_type_id : NULL,
      '#ajax' => [
        'callback' => [static::class, 'ajaxCallback'],
        'wrapper' => 'dmg-guidelines-wrapper',
      ],
    ];
    $form['guidelines'] = isset($entity_type)
      ? [
        '#prefix' => '<div id="dmg-guidelines-wrapper">',
        '#suffix' => '</div>',
        '#type' => 'text_format',
        '#title' => $this->t('Creation Guidelines'),
        '#description' => $this->t('This message will be shown to site builders before they create another display mode. This is often aimed to reduce uncontrolled proliferation of display modes.'),
        '#default_value' => $guidelines,
        '#required' => TRUE,
      ]
      : [
        '#prefix' => '<div id="dmg-guidelines-wrapper">',
        '#suffix' => '</div>',
      ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Callback for updating the creation guidelines on entity type selection.
   */
  public static function ajaxCallback(array &$element, FormStateInterface $form_state): array {
    return $element['guidelines'];
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['dmg.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $type = $form_state->getValue('type');
    $entity_type_id = $form_state->getValue('entity_type_id');
    if (!in_array($type, ['view', 'form'])) {
      // Do not be specific on the error code. This should only happen if the
      // user tampers with the form.
      $form_state->setError($form, $this->t('Invalid option, please try again'));
    }
    if (!$this->entityTypeManager->hasDefinition($entity_type_id)) {
      $form_state->setError($form, $this->t('Invalid option, please try again'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $settings = $this->config('dmg.settings');
    $type = $form_state->getValue('type');
    $entity_type_id = $form_state->getValue('entity_type_id');
    $matches = array_filter(
      $settings->get($type),
      static fn($s) => ($s['entity_type_id'] ?? '') === $entity_type_id,
    );
    $index = key($matches);
    $guidelines = $form_state->getValue(['guidelines', 'value']);
    if (isset($index)) {
      $settings->set(sprintf('%s.%d.guidelines', $type, $index), $guidelines);
    }
    else {
      $value = $settings->get($type);
      $settings->set(
        $type,
        [
          ...$value,
          ['entity_type_id' => $entity_type_id, 'guidelines' => $guidelines],
        ],
      );
    }
    $settings->save();
    parent::submitForm($form, $form_state);
  }

}
